#include "los_swtmr.h"
#include "stdio.h"
#include <stdint.h>
#include "ohos_init.h"

UINT32 g_soft_timer_id;

uint8_t g_count=0;

VOID soft_timer_callback_func(UINT32 para)
{
    printf("para = %d\n",para);
    printf("soft_timer_callback_func %d\n",++g_count);

    if(g_count >= 100){
        LOS_SwtmrStop(g_soft_timer_id);
    }
}

void soft_timer_demo()
{
    printf(">>>>>>>>soft_timer_demo start  \n");

    UINT32 ret = LOS_SwtmrCreate(500,
    LOS_SWTMR_MODE_PERIOD, 
    soft_timer_callback_func,
    &g_soft_timer_id,122);

    if (ret != LOS_OK)
    {
        printf("soft timer create ret=%d\n", ret);
        return;
    }
    printf("soft timer create ok \n", ret);

    ret = LOS_SwtmrStart(g_soft_timer_id);

    if (ret != LOS_OK)
    {
        printf("soft timer start ret=%d\n", ret);
        return;
    }

    printf("soft timer start ok \n", ret);
}

// APP_FEATURE_INIT(soft_timer_demo);
