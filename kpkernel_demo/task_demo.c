
#include "los_task.h"
// #include "ohos_init.h"
#include <stdint.h>

uint32_t task1_id;
/***************************************************************
 * 函数名称: task_one
 * 说    明: 线程函数1
 * 参    数: 无
 * 返 回 值: 无
 ***************************************************************/
void task_one()
{
    while (1)
    {
        printf("This is %s\n", __func__);
        LOS_Msleep(1000);
    }
}

/***************************************************************
 * 函数名称: task_two
 * 说    明: 线程函数2
 * 参    数: 无
 * 返 回 值: 无
 ***************************************************************/
void task_two()
{
    int sum = 0;

    while (1)
    {
        printf("This is %s,sum is%d\n", __func__, sum++);
        LOS_Msleep(3000);

        if (sum >= 10)
        {
            UINT32 task1Status;
            UINT32 ret = LOS_TaskStatusGet(task1_id,
             &task1Status);
            if (ret == LOS_OK)
            {
                if (task1Status != OS_TASK_STATUS_EXIT)
                {
                    LOS_TaskDelete(task1_id);
                }
            }
        }
    }
}

/***************************************************************
 * 函数名称: task_example
 * 说    明: 内核任务例程
 * 参    数: 无
 * 返 回 值: 无
 ***************************************************************/
void task_example()
{

    unsigned int thread_id1;
    unsigned int thread_id2;
    TSK_INIT_PARAM_S task1 = {0};
    TSK_INIT_PARAM_S task2 = {0};
    unsigned int ret = LOS_OK;

    task1.pfnTaskEntry = (TSK_ENTRY_FUNC)task_one;
    task1.uwStackSize = 2048;
    task1.pcName = "Task_One";
    task1.usTaskPrio = 25;
    ret = LOS_TaskCreate(&task1_id, &task1);
    if (ret != LOS_OK)
    {
        printf("ERROR:Falied to create Task_One ret:0x%x\n", ret);
        return;
    }

    task2.pfnTaskEntry = (TSK_ENTRY_FUNC)task_two;
    task2.uwStackSize = 2048;
    task2.pcName = "Task_Two";
    task2.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id2, &task2);
    if (ret != LOS_OK)
    {
        printf("Falied to create Task_Two ret:0x%x\n", ret);
        return;
    }
}

// APP_FEATURE_INIT(task_example);
